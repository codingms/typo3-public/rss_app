# Rss-App Extension for TYPO3

This Plugin enables you to display RSS-App feeds in TYPO3. RSS-App provides a lot of different RSS-Feeds from platforms.


>   #### Attention: {.alert .alert-danger}
>   
>   This TYPO3 extension is based on RSS.app, the No. 1 source for generating RSS feeds. It can create an RSS feed from almost any website or social media service.


**Features:**

*   Use RSS app extension to view RSS feeds from many different platforms in TYPO3.
*   Platforms: Instagram, Twitter, Google News, Reddit, Facebook, YouTube, Telegram, Pinterest, eBay, Tumblr, imgur, Giphy, Unsplash, Scoop.it, NYTimes, CNN, USA Today, BBC, NPR, Washington Post, Vogue, ELLE, Harper's BAZAAR, Vanity Fair, Allure, WWD, Wired, MIT Technology Review, Glamour, InStyle, GQ, Entrepreneur, Fast Company, Fortune
*   Create feeds with a seamless user interface and monitor them with your favorite RSS reader
*   Import to any RSS Reader or export all of your feeds with OPML
*   Most websites and social networks supported
*   The combination of different feeds is also possible
*   Displayable as picture or carousel

If you need some additional or custom feature - get in contact!


**Links:**

*   [Rss-App Documentation](https://www.coding.ms/documentation/typo3-rss-app "Rss-App Documentation")
*   [Rss-App Bug-Tracker](https://gitlab.com/codingms/typo3-public/rss_app/-/issues "Rss-App Bug-Tracker")
*   [Rss-App Repository](https://gitlab.com/codingms/typo3-public/rss_app "Rss-App Repository")
*   [TYPO3 RSS-App Productdetails](https://www.coding.ms/typo3-extensions/typo3-rss-app/ "TYPO3 RSS-App Productdetails")
*   [TYPO3 RSS-App Documentation](https://www.coding.ms/documentation/typo3-rss-app "TYPO3 RSS-App Documentation")
*   [RSS.app Website](https://rss.app/ "RSS.app Website")
