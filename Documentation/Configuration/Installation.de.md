# Installation

1. Installier die Extension mittels composer (```composer req codingms/rss-app```) oder dem Extension-Manager.
2. Füge die statischen Templates der Erweiterung Deinem Basis-Template hinzu.
3. Füge das Plugin "RssApp" auf den Seiten ein, auf denen Du RSS-Feeds anzeigen möchtest.



## Support

Für kostenpflichtigen Support sende eine E-Mail an typo3@coding.ms
