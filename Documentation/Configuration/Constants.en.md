# TypoScript constant settings


## Fluid Templates for RSS-App

| **Constant**      | themes.configuration.extension.rss_app.view.templateRootPath |
| :---------------- | :----------------------------------------------------------- |
| **Label**         | Fluid Templates                                              |
| **Description**   |                                                              |
| **Type**          | string                                                       |
| **Default value** | EXT:rss_app/Resources/Private/Templates/                     |

| **Constant**      | themes.configuration.extension.rss_app.view.partialRootPath  |
| :---------------- | :----------------------------------------------------------- |
| **Label**         | Fluid Partials                                               |
| **Description**   |                                                              |
| **Type**          | string                                                       |
| **Default value** | EXT:rss_app/Resources/Private/Partials/                      |

| **Constant**      | themes.configuration.extension.rss_app.view.layoutRootPath   |
| :---------------- | :----------------------------------------------------------- |
| **Label**         | Fluid Layouts                                                |
| **Description**   |                                                              |
| **Type**          | string                                                       |
| **Default value** | EXT:rss_app/Resources/Private/Layouts/                       |



