# TypoScript-Konstanten Einstellungen


## Fluid Templates für RSS-App

| **Konstante**    | themes.configuration.extension.rss_app.view.templateRootPath |
|:-----------------|:-------------------------------------------------------------|
| **Label**        | Fluid Templates                                              |
| **Beschreibung** |                                                              |
| **Typ**          | string                                                       |
| **Standardwert** | EXT:rss_app/Resources/Private/Templates/                     |

| **Konstante**    | themes.configuration.extension.rss_app.view.partialRootPath  |
|:-----------------|:-------------------------------------------------------------|
| **Label**        | Fluid Partials                                               |
| **Beschreibung** |                                                              |
| **Typ**          | string                                                       |
| **Standardwert** | EXT:rss_app/Resources/Private/Partials/                      |

| **Konstante**    | themes.configuration.extension.rss_app.view.layoutRootPath   |
|:-----------------|:-------------------------------------------------------------|
| **Label**        | Fluid Templates                                              |
| **Beschreibung** |                                                              |
| **Typ**          | string                                                       |
| **Standardwert** | EXT:rss_app/Resources/Private/Layouts/                       |



