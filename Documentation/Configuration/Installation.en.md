# Installation

1. Install the extension using composer (```composer req codingms/rss-app```) or the extension manager.
2. Include the static templates of the extension in your base template.
3. Insert the plugin "RssApp" where you want to display the RSS feed.

## Support

For paid support please send an email to typo3@coding.ms
