<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(
    function () {
        //
        // Plugins
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'RssApp',
            'RssApp',
            'RssApp'
        );
        //
        // Include flex forms
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['rssapp_rssapp'] = 'pi_flexform';
        $flexForm = 'FILE:EXT:rss_app/Configuration/FlexForms/RssApp.xml';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('rssapp_rssapp', $flexForm);
    }
);
