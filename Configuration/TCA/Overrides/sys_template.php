<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(
    function () {
        //
        // Static template
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            'rss_app',
            'Configuration/TypoScript',
            'RSS-App'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            'rss_app',
            'Configuration/TypoScript/OwlCarousel',
            'RSS-App - OwlCarousel'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            'rss_app',
            'Configuration/TypoScript/Stylesheets',
            'RSS-App - Default stylesheets'
        );
    }
);
