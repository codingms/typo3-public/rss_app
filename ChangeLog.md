# RSS-App Change-Log

*	[TASK] Migrate TypoScript imports



## 2023-11-01  Release of version 3.0.2

*	[TASK] Clean up documentation



## 2023-10-18  Release of version 3.0.1

*	[BUGFIX] Fix image display for RSS feed



## 2023-10-18  Release of version 3.0.0

*	[TASK] Migrate to TYPO3 12 and remove support for TYPO3 10
*	[TASK] Optimized documentation features



## 2023-02-03 Release of version 2.0.3

*	[BUGFIX] Avoid displaying warnings
*	[TASK] Migrate ext_tables and ext_localconf
*	[TASK] Optimized documentation



## 2022-07-04 Release of version 2.0.2

*	[TASK] Move Owl-Carousel JavaScript into page.includeJSFooterlibs



## 2022-06-27 Release of version 2.0.1

*	[BUGFIX] Fix access on storage repository



## 2022-06-27 Release of version 2.0.0

*	[TASK] Migration for TYPO3 11 and PHP 8.0
*	[TASK] Preparations for TYPO3 11



## 2022-03-26 Release of version 1.0.7

*	[BUGFIX] Cache instagram images
*	[TASK] Add documentations configuration



## 2021-05-15 Release of version 1.0.6

*	[TASK] Add extension key in composer.json



## 2020-12-15 Release of version 1.0.5

*	[TASK] Add translation files see #1
*	[TASK] Check if node properties exist before accessing them
*	[TASK] Add note on installation in the readme files



## 2020-10-04 Release of version 1.0.4

*	[TASK] Add extra tags in composer.json



## 2020-10-04 Release of version 1.0.3

*	[BUGFIX] Rise additional_tca requirement



## 2020-08-20 Release of Version 1.0.2

*	[TASK] Add extra tags in composer.json
*	[TASK] Add additional_tca support



## 2020-06-17 Release of Version 1.0.1

*	[BUGFIX] Fix items without images



## 2020-06-14 Release of Version 1.0.0

*	[TASK] Implement base features
