# Rss-App Erweiterung für TYPO3

Mit diesem Plugin kannst Du RSS-App-Feeds in TYPO3 anzeigen. Die RSS-App bietet viele verschiedene RSS-Feeds von Plattformen.


>   #### Achtung: {.alert .alert-danger}
>   
>   Diese TYPO3 Extension basiert auf RSS.app, der Nr. 1 Source zur Generierung von RSS-Feeds. Sie kann von fast jeder Website sowie Social Media Dienst einen RSS Feed erstellen.


**Features:**

*   Verwende die RSS-App-Erweiterung, um RSS-Feeds von von vielen verschiedenen Plattformen in TYPO3 anzuzeigen.
*   Plattformen: Instagram, Twitter, Google News, Reddit, Facebook, YouTube, Telegram, Pinterest, eBay, Tumblr, imgur, Giphy, Unsplash, Scoop.it, NYTimes, CNN, USA Today, BBC, NPR, Washington Post, Vogue, ELLE, Harper's BAZAAR, Vanity Fair, Allure, WWD, Wired, MIT Technology Review, Glamour, InStyle, GQ, Entrepreneur, Fast Company, Fortune
*   Erstelle RSS Feeds und überwache sie mit Deinem bevorzugtem RSS-Reader
*   Importiere in einen beliebigen RSS-Reader oder exportiere alle Deine Feeds via OPML
*   Die meisten Websites und sozialen Netzwerke werden unterstützt
*   Auch die Kombination verschiedener Feeds ist möglich
*   Die Darstellung erfolgt als Bild oder Carousel

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Rss-App Dokumentation](https://www.coding.ms/documentation/typo3-rss-app "Rss-App Dokumentation")
*   [Rss-App Bug-Tracker](https://gitlab.com/codingms/typo3-public/rss_app/-/issues "Rss-App Bug-Tracker")
*   [Rss-App Repository](https://gitlab.com/codingms/typo3-public/rss_app "Rss-App Repository")
*   [TYPO3 RSS-App Produktdetails](https://www.coding.ms/de/typo3-extensions/typo3-rss-app/ "TYPO3 RSS-App Produktdetails")
*   [TYPO3 RSS-App Dokumentation](https://www.coding.ms/de/dokumentation/typo3-rss-app "TYPO3 RSS-App Dokumentation")
*   [RSS.app Website](https://rss.app/ "RSS.app Website")
