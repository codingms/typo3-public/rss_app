<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(
    function()
    {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'RssApp',
            'RssApp',
            [\CodingMs\RssApp\Controller\RssAppController::class => 'show'],
            [\CodingMs\RssApp\Controller\RssAppController::class => '']
        );
        //
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1585186052] = [
            'nodeName' => 'RssAppRow',
            'priority' => '70',
            'class' => \CodingMs\RssApp\Form\Element\RssAppRow::class,
        ];

        //
        // register svg icons: identifier and filename
        $iconsSvg = [
            'content-plugin-rssapp-rssapp' => 'Resources/Public/Icons/iconmonstr-rss-feed-4.svg',
        ];
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        foreach ($iconsSvg as $identifier => $path) {
            $iconRegistry->registerIcon(
                $identifier,
                \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                ['source' => 'EXT:rss_app/' . $path]
            );
        }
        //
        // Backend TypoScript
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            '@import "EXT:rss_app/Configuration/PageTS/tsconfig.typoscript"'
        );
    }
);
